package fr.dawan.springcore;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import fr.dawan.springcore.beans.Adresse;
import fr.dawan.springcore.beans.Stagiaire;

//Configuration avec java (à priviligier avec spring boot)
@Configuration // annotation pour indiquer que la classe ConfigJava est une classe de configuration
@Import(AdresseJavaConfig.class) // Import des beans de la classe de configuration AdresseJavaConfig.class
@ComponentScan("fr.dawan.springcore") // choix du package à scanner pour découvrir des composants
public class ConfigJava {

    // @Bean -> Déclaration d'un bean
    // le nom du bean (id,name en xml) = le nom de la méthode, on peut aussi le préciser avec l'attribut name de @Bean
    // La classe (class en xml) du bean correspont au type de retour de la méthode
   @Bean
   public Stagiaire stagiaire1() {
       return new Stagiaire();
   }
    
   // On peut matérialiser les dépendances avec les paramètres de la méthode
   // ici lien explicite avec le bean qui pour nom adresse2 (équivalent à ref en xml)
    @Bean
    public Stagiaire stagiaire2(Adresse adresse1) {
        return new Stagiaire("John","Doe",adresse1);
    }

}
