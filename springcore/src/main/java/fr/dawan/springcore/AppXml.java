package fr.dawan.springcore;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.dawan.springcore.beans.Stagiaire;

public class AppXml {
    public static void main(String[] args) {
        // Création du conteneur
        ApplicationContext ctx = new ClassPathXmlApplicationContext("configxml.xml");

        // getBean permet de récupérer les instances des beans depuis le conteneur
        Stagiaire st1 = ctx.getBean("stagiaire1", Stagiaire.class);
        System.out.println(st1);

        // Fermer le conteneur
        ((AbstractApplicationContext) ctx).close();
    }
}
