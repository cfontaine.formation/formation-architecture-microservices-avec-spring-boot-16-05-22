package fr.dawan.springcore.beans;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Repository;

@Repository
public class StagiaireRepository {

    @PostConstruct
    public void init() {
        System.out.println("repository init");
    }
    
}
