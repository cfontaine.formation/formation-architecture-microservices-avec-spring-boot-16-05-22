package fr.dawan.springcore.beans;

import java.io.Serializable;

public class Adresse implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String rue;
    
    private String ville;

    public Adresse() {
        super();
    }

    public Adresse(String rue, String ville) {
        this.rue = rue;
        this.ville = ville;
    }

    public String getRue() {
        return rue;
    }

    public void setRue(String rue) {
        this.rue = rue;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    @Override
    public String toString() {
        return "Adresse [rue=" + rue + ", ville=" + ville + ", toString()=" + super.toString() + "]";
    }
    
    

}
