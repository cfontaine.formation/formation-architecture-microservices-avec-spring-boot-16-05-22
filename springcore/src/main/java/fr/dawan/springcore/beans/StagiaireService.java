package fr.dawan.springcore.beans;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

@Service(value="servicesta")

//@Lazy
//@Scope("prototype")
public class StagiaireService {
    
    @PostConstruct
    public void init() {
        System.out.println("service init");
    }
    
    public List<Stagiaire> getAllstagiaire() {
        return null;
    }

}
