package fr.dawan.springcore.beans;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class Stagiaire implements Serializable {

    private static final long serialVersionUID = 1L;

    private String prenom;

    private String nom;

    @Autowired
    @Qualifier("adresse2")
    private Adresse adresse;

    public Stagiaire() {
    }

    public Stagiaire(String prenom, String nom) {

        this.prenom = prenom;
        this.nom = nom;
    }

    // @Autowired
    public Stagiaire(String prenom, String nom, /* @Qualifier("adresse2") */ Adresse adresse) {
        this(prenom, nom);
        this.adresse = adresse;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Adresse getAdresse() {
        return adresse;
    }

    // @Autowired
    public void setAdresse(/* @Qualifier("adresse2") */Adresse adresse) {
        this.adresse = adresse;
    }

    @Override
    public String toString() {
        return "Stagiaire [prenom=" + prenom + ", nom=" + nom + ", adresse=" + adresse + "]";
    }

    @PostConstruct
    public void init() {
        System.out.println("Méthode init Stagiaire");
    }

    @PreDestroy
    public void destroy() {
        System.out.println("Méthode destroy Stagiaire");
    }

}
