package fr.dawan.springcore;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import fr.dawan.springcore.beans.Adresse;
@Configuration
public class AdresseJavaConfig {
    
    @Bean
    public Adresse adresse1() {
        return new Adresse();
    }

    @Bean
    public Adresse adresse2() {
        return new Adresse();
    }
}
