package fr.dawan.springcore;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import fr.dawan.springcore.beans.Stagiaire;
import fr.dawan.springcore.beans.StagiaireService;

public class AppJava {

    public static void main(String[] args) {
        ApplicationContext ctx=new AnnotationConfigApplicationContext(ConfigJava.class);
        System.out.println("-----------------------");
        Stagiaire st1=ctx.getBean("stagiaire1",Stagiaire.class);
        System.out.println(st1);
        
        Stagiaire st2=ctx.getBean("stagiaire2",Stagiaire.class);
        System.out.println(st2);
        
        StagiaireService sts=ctx.getBean("servicesta",StagiaireService.class);
        System.out.println(sts);
        
        StagiaireService sts1=ctx.getBean("servicesta",StagiaireService.class);
        System.out.println(sts1);
        ((AbstractApplicationContext)ctx).close();
    }

}
