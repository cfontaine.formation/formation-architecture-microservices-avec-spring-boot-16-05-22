package fr.dawan.springboot.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;

@Entity
public class Pays extends AbstractEntity {

    private static final long serialVersionUID = 1L;
    
    private String nom;
    
    @ManyToMany
    private List<Entreprise> entreprises=new ArrayList<>();

    public Pays() {
    }

    public Pays(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    
    
}
