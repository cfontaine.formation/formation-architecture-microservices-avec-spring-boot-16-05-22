package fr.dawan.springboot.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import fr.dawan.springboot.enums.Contrat;

@Entity
@Table(name="employes")
//@SequenceGenerator(name="employegenerator")
public class Employe extends AbstractEntity{

    private static final long serialVersionUID = 1L;

//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private long id;
    
    @Column(length = 50, nullable = false)
    private String prenom;
    
    @Column(length = 50,nullable = false)
    private String nom;
    
    @Enumerated(EnumType.STRING)
    @Column(length = 15,nullable = false)
    private Contrat contrat;
    
    @Transient
    private String nePasPersister;
    
    @Embedded
    private Adresse adressePerso;
    
    @ElementCollection(targetClass = String.class)
    @CollectionTable(name="employe_telephone")
    private List<String> telephone=new ArrayList<>();
    
    @Embedded
    @AttributeOverrides({
        @AttributeOverride(
                name="rue", column = @Column(name="rue_travail")),
        @AttributeOverride(
                name="ville", column = @Column(name="ville_travail")),
        @AttributeOverride(
                name="codePostal", column = @Column(name="code_postal_travail"))
    
    })
    private Adresse adresseTravail;
    
    @ManyToOne
    private Entreprise entreprise;

    public Employe() {

    }

    public Employe(String prenom, String nom) {
        this.prenom = prenom;
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    
//    public long getId() {
//        return id;
//    }
//
//    public void setId(long id) {
//        this.id = id;
//    }

    @Override
    public String toString() {
        return "Employe [prenom=" + prenom + ", nom=" + nom + "]";
    }
}
