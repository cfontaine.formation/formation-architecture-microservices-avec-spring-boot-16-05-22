package fr.dawan.springboot.entities;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "maires")
public class Maire extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    private String prenom;

    private String nom;

    @OneToOne(mappedBy = "maire")
    private Ville ville;

    public Maire() {
    }

    public Maire(String prenom, String nom) {
        this.prenom = prenom;
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

}
