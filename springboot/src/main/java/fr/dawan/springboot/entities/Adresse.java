package fr.dawan.springboot.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Adresse {

    @Column(nullable = false)
    private String rue;

    @Column(nullable = false, length = 60)
    private String ville;

    @Column(name="code_postal",nullable = false, length = 8)
    private String codePostal;

    public Adresse(String rue, String ville, String codePostal) {
        super();
        this.rue = rue;
        this.ville = ville;
        this.codePostal = codePostal;
    }

    public String getRue() {
        return rue;
    }

    public void setRue(String rue) {
        this.rue = rue;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    @Override
    public String toString() {
        return "Adresse [rue=" + rue + ", ville=" + ville + ", codePostal=" + codePostal + "]";
    }
}
