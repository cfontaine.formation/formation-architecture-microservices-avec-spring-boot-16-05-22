package fr.dawan.springboot.entities;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="villes")
public class Ville extends AbstractEntity {

    private static final long serialVersionUID = 1L;
    
    private String nom;
    
    @OneToOne
    private Maire maire;

    public Ville() {

    }

    public Ville(String nom) {
        super();
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    
    
}
