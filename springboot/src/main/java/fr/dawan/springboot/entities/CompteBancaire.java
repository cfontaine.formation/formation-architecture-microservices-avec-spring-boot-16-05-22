package fr.dawan.springboot.entities;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
//@DiscriminatorColumn(name = "type_compte", length = 2)
//@DiscriminatorValue(value = "B")
public class CompteBancaire {
    
    @Id
    @GeneratedValue
    private long id;

    private String titulaire;
    
    private String iban;
    
    private double solde;

    public CompteBancaire() {

    }

    public CompteBancaire(String titulaire, String iban, double solde) {
        this.titulaire = titulaire;
        this.iban = iban;
        this.solde = solde;
    }
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitulaire() {
        return titulaire;
    }

    public void setTitulaire(String titulaire) {
        this.titulaire = titulaire;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public double getSolde() {
        return solde;
    }

    public void setSolde(double solde) {
        this.solde = solde;
    }

    @Override
    public String toString() {
        return "CompteBancaire [titulaire=" + titulaire + ", iban=" + iban + ", solde=" + solde + "]";
    }
    
}
