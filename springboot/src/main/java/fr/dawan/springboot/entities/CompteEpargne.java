package fr.dawan.springboot.entities;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
@Entity
//@DiscriminatorValue(value="E")
public class CompteEpargne extends CompteBancaire {

    private double taux=1.5;

    public CompteEpargne() {
        super();
    }

    public CompteEpargne(String titulaire, String iban, double solde,double taux) {
        super(titulaire, iban, solde);
        this.taux=taux;
    }

    public double getTaux() {
        return taux;
    }

    public void setTaux(double taux) {
        this.taux = taux;
    }

    @Override
    public String toString() {
        return "CompteEpargne [taux=" + taux + ", toString()=" + super.toString() + "]";
    }

}
