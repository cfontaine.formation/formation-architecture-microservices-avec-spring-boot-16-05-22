package fr.dawan.springboot.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "entreprises")
public class Entreprise extends AbstractEntity {

    private static final long serialVersionUID = 1L;
    
    private String nom;
    
    @OneToMany(mappedBy = "entreprise")
    private List<Employe> employes=new ArrayList<>();
    
    @ManyToMany(mappedBy="entreprises")
    private List<Pays> pays=new ArrayList<>();

    public Entreprise() {

    }

    public Entreprise(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    
    

}
