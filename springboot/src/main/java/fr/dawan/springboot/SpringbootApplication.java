package fr.dawan.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootApplication.class, args);
		
//	    SpringApplication sba=new SpringApplication(SpringbootApplication.class);
//	    sba.setBannerMode(Banner.Mode.OFF);
//	    sba.run(args);
	}
}
